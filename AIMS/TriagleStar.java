import java.util.Scanner;

import javax.swing.JOptionPane;
public class TriagleStar {
    public static void main(String[] args) {
        int n;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter n = ");
        n = scanner.nextInt();
        for(int i = 1; i <= n; i++)
        {
            for(int j = 1; j <= 1+(n-1)*2; j++)
            {
                int mid = (1 + (n - 1)*2)/2 + 1 ;
                if(j < mid + i && j > mid - i){
                    System.out.print("*");
                }
                else{
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
