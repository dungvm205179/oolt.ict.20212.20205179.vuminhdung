public class Aims {
    public static void main(String[] args) {
        Order anOrder = new Order();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("Harry Potter");
        dvd1.setCategory("Fantasy");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Big bang");
        dvd2.setCategory("Science");
        dvd2.setCost(255f);
        dvd2.setDirector("Jack Sollof");
        dvd2.setLength(100);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("How it happened");
        dvd3.setCategory("Science");
        dvd3.setCost(152f);
        dvd3.setDirector("Jonas Brothers");
        dvd3.setLength(100);

        DigitalVideoDisc[] dvdList = new DigitalVideoDisc[] { dvd1, dvd2 };
        anOrder.addDigitalVideoDisc(dvdList);

        System.out.println("You currently have " + anOrder.qtyOrdered + " dics in your order");
        System.out.println("Total cost is " + anOrder.totalCost());

        anOrder.removeDigitalVideoDisc(dvd1);
        System.out.println("You currently have " + anOrder.qtyOrdered + " dics in your order");
        System.out.println("Total cost after removing dvd1 is " + anOrder.totalCost());

        System.out.println();
        anOrder.printListOfOrder();

        Order anOrder2 = new Order();
        anOrder2.addDigitalVideoDisc(dvd3);
        anOrder2.printListOfOrder();
    }
}
