import javax.swing.event.SwingPropertyChangeSupport;
import java.util.zip.InflaterOutputStream;
import javax.swing.JOptionPane;
import java.lang.Math;
public class Solution {
    public static void FirstDegree(double a, double b, double c)
    {
        if(a == 0 && b == c){
            String Result = "The equation has infinitely many solutions.";
            JOptionPane.showMessageDialog(null, Result);
        }else if(a == 0 && b != c){
            String Result = "The equation has no solutions.";
            JOptionPane.showMessageDialog(null, Result); 
        }else{
            String Result = "The equation has one root: " + String.valueOf((c-b)/a);
            JOptionPane.showMessageDialog(null, Result);
        }
    }

    public static void LinearEquation(double a11, double a12, double b1, double a21, double a22, double b2 )
    {
        double D = a11 * a22 - a21 * a12;
        double D1 = b1 * a22 - b2 * a12;
        double D2 = a11 * b2 - a21 * b1;
        if(D != 0){
            double x1 = D1 / D;
            double x2 = D2 / D;
            String Result = "The equation has a unique solution: (x1,x2) = " + "(" + String.valueOf(x1) + "," + String.valueOf(x2) + ")";
            JOptionPane.showMessageDialog(null, Result);
        }else if(D == 0 && D1 == 0 && D2 == 0){
            String Result = "The equation has infinitely many solutions.";
            JOptionPane.showMessageDialog(null, Result);
        }else{
            String Result = "The equation has no solution.";
            JOptionPane.showMessageDialog(null, Result);
        }
    }

    public static void SecondDegree(double a, double b, double c)
    {
        double delta = b * b - 4 * a * c;
        if(delta == 0)
        {
            double x = -(b / (2 * a));
            String Result = "The equation has one solution: x = " + String.valueOf(x);
            JOptionPane.showMessageDialog(null, Result);
        }else if(delta < 0){
            String Result = "The equation has no solution. ";
            JOptionPane.showMessageDialog(null, Result);
        }else{
            double x1 = (b * (-1) + Math.sqrt(delta)) / (2 * a);
            double x2 = (b * (-1) - Math.sqrt(delta)) / (2 * a);
            String Result = "The equation has 2 solutions: " + "x1 = " + String.valueOf(x1) + "and x2 = " + String.valueOf(x2);
            JOptionPane.showMessageDialog(null, Result);
        }
    }

    public static void main(String args[])
    {
        String choose;
        do{
            choose = JOptionPane.showInputDialog(null, "Please choose what you want to solve \n1.First-Degree equation. \n2.Linear Equation. \n3.Second-Degree equation. \n4.Exit ");
            switch(choose)
            {
                case "1":
                {
                    double a, b, c;
                    JOptionPane.showMessageDialog(null, "The equation has this form: ax + b = c");
                    String sa = JOptionPane.showInputDialog(null, "Please Enter a: ", "Input a", JOptionPane.INFORMATION_MESSAGE);
                    String sb = JOptionPane.showInputDialog(null, "Please Enter b: ", "Input b", JOptionPane.INFORMATION_MESSAGE);
                    String sc = JOptionPane.showInputDialog(null, "Please Enter c: ", "Input c", JOptionPane.INFORMATION_MESSAGE);
                    a = Double.parseDouble(sa);
                    b = Double.parseDouble(sb);
                    c = Double.parseDouble(sc);
                    FirstDegree(a, b, c);
                    break;
                }

                case "2":
                {
                    double a11, a12, b1, a21, a22, b2;
                    JOptionPane.showMessageDialog(null, "The system has this form: \na11 * x1 + a12 * x2 = b1\na21 * x1 + a22 * x2 = b2");
                    String sa11 = JOptionPane.showInputDialog(null, "Please Enter a11: ");
                    String sa12 = JOptionPane.showInputDialog(null, "Please Enter a12: ");
                    String sb1 = JOptionPane.showInputDialog(null, "Please Enter b1: ");
                    String sa21 = JOptionPane.showInputDialog(null, "Please Enter a21: ");
                    String sa22 = JOptionPane.showInputDialog(null, "Please Enter a22: ");
                    String sb2 = JOptionPane.showInputDialog(null, "Please Enter b2: ");
                    a11 = Double.parseDouble(sa11);
                    a12 = Double.parseDouble(sa12);
                    b1 = Double.parseDouble(sb1);
                    a21 = Double.parseDouble(sa21);
                    a22 = Double.parseDouble(sa22);
                    b2 = Double.parseDouble(sb2);
                    LinearEquation(a11, a12, b1, a21, a22, b2);
                    break;
                }
                
                case "3":
                {
                    double a, b, c;
                    JOptionPane.showMessageDialog(null, "The equation has this form: ax^2 + bx + c = 0");
                    String sa = JOptionPane.showInputDialog(null, "Please Enter a: ");
                    String sb = JOptionPane.showInputDialog(null, "Please Enter b: ");
                    String sc = JOptionPane.showInputDialog(null, "Please Enter c: ");
                    a = Double.parseDouble(sa);
                    b = Double.parseDouble(sb);
                    c = Double.parseDouble(sc);
                    SecondDegree(a, b, c);
                    break;
                }
                
                case "4":
                {
                    System.exit(0);
                }
            }
        }while(choose != "4");
    }
}
